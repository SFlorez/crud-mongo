## Technologies
Project is created with:
* Node
* Npm
* Express
* mongoose
* Joi
* multer
* Cloudinary
* log4js
* nodemon

## Setup
Configure the environment variables:

```bash
$ cd <project-path> 
# Replace `<project-path>` with path of the project.
$ cp .env.example .env
```

Modify the env variables, ex:

```
SERVER_PORT=3001
NODE_ENV=development
BASE_URL=http://localhost

CRUD_DB_HOST=subdomain.mydomian.com
CRUD_DB_PORT=12345
CRUD_DB_NAME=mydb
CRUD_DB_PASSWORD=dbpass
CRUD_DB_USER=dbuser

CRUD_ENABLE_CLOUDINARY=true
CRUD_CLOUDINARY_CLOUD_NAME=blog
CRUD_CLOUDINARY_API_KEY=0000000000
CRUD_CLOUDINARY_API_SECRET=APi-SeCreT
```

To run this project, install dependencies using npm:

```
$ npm install
$ npm run dev
```

Server default run in [http://localhost:3001](http://localhost:3001) change the port if necesary.

## Available Scripts

In the project directory, you can run:

### `npm run dev`

Runs the server in the development mode.<br />
Open [http://localhost:3001](http://localhost:3001) to view it in the browser.

The server will reload if you make edits.<br />
You will also see any lint errors in the console.

## Instructions

* [Documentation](./doc/restdoc.md): REST API Docs (./doc/restdoc.md)