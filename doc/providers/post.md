# Create Provider

Create an Provider.

**URL** : `/api/v1/providers/`

**Method** : `POST`

**Auth required** : NO

**Permissions required** : None

**Data constraints**

```json
{
    "firstName": [ "String", "Required" ],
    "lastName": [ "String", "Required" ],
    "email": [ "Email", "Unique in Providers", "Required" ],
    "specialty": [ "object", "required", {
        "_id": [ "Object Id", "Required" ],
        "name": [ "String", "Required" ],
        "createdBy": [ "Integer", "Required" ],
        "createdAt": [ "Date", "Required" ],
        "updatedBy": [ "Integer", "Required" ],
        "updatedAt": [ "Date", "Required" ]
    }],
    "projectedStartDate": [ "Date", "Required" ],
    "providerType": [ "Enum", "Required", { "possibleValues": [
        "APRN",
        "ARNP",
        "CNS",
        "CRNA",
        "DC",
        "DDS",
        "DMD",
        "DO",
        "DPM",
        "LCMFT",
        "LCMHC",
        "LCP",
        "MD",
        "NP",
        "PA"
    ]}],
    "staffStatus": [ "Enum", "Required", { "possibleValues": [
        "ACTIVE",
        "AFFILIATE",
        "ASSOCIATE",
        "COMMUNITY",
        "CONSULTING",
        "COURTESY",
        "FACULTY",
        "HONORARY",
        "HOSPITALIST",
        "HOUSE_STAFF",
        "LOCUM_TENENS",
        "PROVISIONAL",
        "RESINDENT",
        "TEACHING"
    ]}],
    "status": [ "Enum", "Required", { "possibleValues": [
        "AWAITING_CREDENTIALS",
        "READY_FOR_REVIEW",
        "UNDER_REVIEW",
        "AWAITING_DECISION",
        "APPROVED",
        "DENIED"
    ], "dafaultValue": "AWAITING_CREDENTIALS"  }],
    "profilePhoto": [ "Uri" ],
    "employerId": [ "Integer" ],
    "assignedTo": [ "Integer", "Required when empoyerId exist" ],
    "createdBy": [ "Integer", "Required" ]
}
```

**Data example** Only required fields.

```json
{
    "firstName": "Name4",
    "lastName": "LastName",
    "email": "test4@hotmail.com",
    "specialty": {
    	"_id": "00000000000000001",
        "name": "Name for test",
        "createdBy": 12345,
        "createdAt": "2020-04-11T15:54:20.898Z",
        "updatedBy": 12345,
        "updatedAt": "2020-04-11T15:54:20.898Z"
    },
    "projectedStartDate": "2019-12-17T16:48:21.620Z",
    "status": "READY_FOR_REVIEW",
    "providerType": "NP",
    "staffStatus": "COMMUNITY",
    "createdBy": 01234
}
```

## Success Response

**Condition** : If everything is OK.

**Code** : `201 CREATED`

**Content example**

```json
{
    "success": true,
    "item": {
        "status": "READY_FOR_REVIEW",
        "_id": "00000000000000000000005",
        "firstName": "Name4",
        "lastName": "LastName",
        "email": "test4@hotmail.com",
        "specialty": {
            "_id": "00000000000000001",
            "name": "Name for test",
            "createdBy": 12345,
            "createdAt": "2020-04-11T15:54:20.898Z",
            "updatedBy": 12345,
            "updatedAt": "2020-04-11T15:54:20.898Z"
        },
        "projectedStartDate": "2021-12-17T16:48:21.620Z",
        "providerType": "NP",
        "staffStatus": "COMMUNITY",
        "createdBy": 01234,
        "createdAt": "2020-04-12T02:47:13.460Z",
        "updatedBy": 01234,
        "updatedAt": "2020-04-12T02:47:13.460Z",
        "extraField": "PENDING"
    }
}
```

### OR

**Data example** 

* All Fields with `multipart/form-data` content-type the `specialty` field accept text... in backend parse this to JSON for validate.

* With `application/json` content-type work with json.

![Body multipar form data](./post.png)

**Condition** : If everything is OK.

**Code** : `201 CREATED`

**Content example**

```json
{
    "success": true,
    "item": {
        "status": "READY_FOR_REVIEW",
        "_id": "000000000000000004",
        "firstName": "Name3",
        "lastName": "LastName",
        "email": "test7@hotmail.com",
        "projectedStartDate": "2021-12-17T16:48:21.620Z",
        "providerType": "NP",
        "staffStatus": "COMMUNITY",
        "employerId": 123,
        "createdBy": 78895,
        "assignedTo": 123456,
        "specialty": {
            "_id": "00000000000000001",
            "name": "Name for test",
            "createdBy": 12345,
            "createdAt": "2020-04-11T15:54:20.898Z",
            "updatedBy": 12345,
            "updatedAt": "2020-04-11T15:54:20.898Z"
        },
        "createdAt": "2020-04-12T03:12:31.764Z",
        "updatedBy": 123456,
        "updatedAt": "2020-04-12T03:12:31.764Z",
        "profilePhoto": "https://res.cloudinary.com/uri-example/2020-04-12T03:12:31.764Z.png",
        "extraField": "PENDING"
    }
}
```

## Error Responses

**Condition** : One field requred is ignored.

**Code** : `422 UNPROCESSABLE ENTITY`

**Content** : 

```json
{
    "success": false,
    "error": {
        "isJoi": true,
        "name": "ValidationError",
        "details": [
            {
                "message": "\"providerType\" is required",
                "path": [
                    "providerType"
                ],
                "type": "any.required",
                "context": {
                    "key": "providerType",
                    "label": "providerType"
                }
            }
        ],
        "_object": {
            "firstName": "Name4",
            "lastName": "LastName",
            "email": "test4@hotmail.com",
            "specialty": {
                "_id": "00000000000000001",
                "name": "Name for test",
                "createdBy": 12345,
                "createdAt": "2020-04-11T15:54:20.898Z",
                "updatedBy": 12345,
                "updatedAt": "2020-04-11T15:54:20.898Z"
            },
            "projectedStartDate": "2019-12-17T16:48:21.620Z",
            "status": "READY_FOR_REVIEW",
            "providerType": "NP",
            "staffStatus": "COMMUNITY",
            "createdBy": 01234
        }
    }
}
```

### Or

**Condition** : Field enum type error.

**Code** : `422 UNPROCESSABLE ENTITY`

**Content** : 

```json
{
    "success": false,
    "error": {
        "isJoi": true,
        "name": "ValidationError",
        "details": [
            {
                "message": "\"status\" must be one of [AWAITING_CREDENTIALS, READY_FOR_REVIEW, UNDER_REVIEW, AWAITING_DECISION, APPROVED, DENIED]",
                "path": [
                    "status"
                ],
                "type": "any.allowOnly",
                "context": {
                    "value": "OPTION_UNLISTED",
                    "valids": [
                        "AWAITING_CREDENTIALS",
                        "READY_FOR_REVIEW",
                        "UNDER_REVIEW",
                        "AWAITING_DECISION",
                        "APPROVED",
                        "DENIED"
                    ],
                    "key": "status",
                    "label": "status"
                }
            }
        ],
        "_object": {
            "firstName": "Name3",
            "lastName": "LastName",
            "email": "test7@hotmail.com",
            "status": "OPTION_UNLISTED",
            ...
        }
    }
}
```

### Or

**Condition** : Email repeated.

**Code** : `422 UNPROCESSABLE ENTITY`

**Content** : 

```json
{
    "success": false,
    "error": {
        "isJoi": true,
        "name": "ValidationError",
        "details": [
            {
                "message": "\"email\" test7@hotmail.com must by unique in Providers",
                "path": [
                    "email"
                ],
                "type": "extendString.uniqueIn",
                "context": {
                    "v": "test7@hotmail.com",
                    "m": "Providers",
                    "key": "email",
                    "label": "email"
                }
            }
        ],
        "_object": {
            "firstName": "Name3",
            "lastName": "LastName",
            "email": "test7@hotmail.com",
            ...
        }
    }
}
```

### Or

**Condition** : Select non-image file.

**Code** : `422 UNPROCESSABLE ENTITY`

**Content** :

```json
{
    "success": false,
    "error": "Invalid file type, only JPG, JPEG and PNG is allowed!"
}
```