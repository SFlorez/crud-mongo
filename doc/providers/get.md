# Show Providers

Show all Providers with pagination optional.

**URL** : `/api/v1/providers/`

**Query Parameters** : 

  * `pageSize=[integer]` where `pageSize` is the number of elements per page returned.
  * `pageIndex=[integer]` where `pageIndex` is the number of the current page.

**Method** : `GET`

**Auth required** : NO

**Permissions required** : None

**Data constraints** : `{}`

## Success Responses

**Condition** : User can see all items if he does not send the page query.

**Code** : `200 OK`

**Content** : 

```json
{
    "success": true,
    "totalItems": 155,
    "pageSize": 1,
    "pageIndex": 38,
    "items": [
        {
            "_id": "0000000000000000001",
            "firstName": "Name",
            "lastName": "LastName",
            "email": "test1@email.com",
            "specialty": {
                "_id": "00000000000000003",
                "name": "Name 3",
                "createdBy": 123,
                "createdAt": "2018-02-14T23:50:21.903Z",
                "updatedBy": 1234,
                "updatedAt": "2018-09-12T05:37:40.170Z"
            },
            "projectedStartDate": "2016-12-24T05:56:25.515Z",
            "employerId": 123,
            "providerType": "DO",
            "staffStatus": "COMMUNITY",
            "assignedTo": 12345,
            "status": "READY_FOR_REVIEW",
            "createdBy": 12345,
            "createdAt": "2017-04-22T11:21:14.160Z",
            "updatedBy": 12345,
            "updatedAt": "2017-09-12T05:18:07.647Z",
            "extraField": "DONE"
        },
        {
            "_id": "0000000000000000003",
            "firstName": "Name",
            "lastName": "LastName",
            "email": "test2@email.com",
            "specialty": {
                "_id": "00000000000000003",
                "name": "Name 3",
                "createdBy": 123,
                "createdAt": "2018-02-14T23:50:21.903Z",
                "updatedBy": 1234,
                "updatedAt": "2018-09-12T05:37:40.170Z"
            },
            "projectedStartDate": "2017-02-26T07:41:39.957Z",
            "employerId": 123,
            "providerType": "NP",
            "staffStatus": "ACTIVE",
            "assignedTo": 12345,
            "status": "AWAITING_DECISION",
            "createdBy": 1234,
            "createdAt": "2017-04-24T13:35:47.170Z",
            "updatedBy": 1234,
            "updatedAt": "2017-09-12T10:50:44.900Z",
            "extraField": "DONE"
        },
        ...
    ]
}
```


### OR

**Condition** : Request with pagination parameters `pageSize` and `pageIndex`.

**Code** : `200 OK`

**Content** : Query structure: `/api/v1/providers?pageSize=2&pageIndex=1`:

```json
{
    "success": true,
    "totalItems": 155,
    "pageSize": 1,
    "pageIndex": 38,
    "items": [
        {
            "_id": "0000000000000000001",
            "firstName": "Name",
            "lastName": "LastName",
            "email": "test1@email.com",
            "specialty": {
                "_id": "00000000000000003",
                "name": "Name 3",
                "createdBy": 123,
                "createdAt": "2018-02-14T23:50:21.903Z",
                "updatedBy": 1234,
                "updatedAt": "2018-09-12T05:37:40.170Z"
            },
            "projectedStartDate": "2016-12-24T05:56:25.515Z",
            "employerId": 123,
            "providerType": "DO",
            "staffStatus": "COMMUNITY",
            "assignedTo": 12345,
            "status": "READY_FOR_REVIEW",
            "createdBy": 12345,
            "createdAt": "2017-04-22T11:21:14.160Z",
            "updatedBy": 12345,
            "updatedAt": "2017-09-12T05:18:07.647Z",
            "extraField": "DONE"
        },
        {
            "_id": "0000000000000000003",
            "firstName": "Name",
            "lastName": "LastName",
            "email": "test2@email.com",
            "specialty": {
                "_id": "00000000000000003",
                "name": "Name 3",
                "createdBy": 123,
                "createdAt": "2018-02-14T23:50:21.903Z",
                "updatedBy": 1234,
                "updatedAt": "2018-09-12T05:37:40.170Z"
            },
            "projectedStartDate": "2017-02-26T07:41:39.957Z",
            "employerId": 123,
            "providerType": "NP",
            "staffStatus": "ACTIVE",
            "assignedTo": 12345,
            "status": "AWAITING_DECISION",
            "createdBy": 1234,
            "createdAt": "2017-04-24T13:35:47.170Z",
            "updatedBy": 1234,
            "updatedAt": "2017-09-12T10:50:44.900Z",
            "extraField": "DONE"
        }
    ]
}
```