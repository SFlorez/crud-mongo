# Update Provider

Update a single Provider.

**URL** : `/api/v1/specialties/:id/`

**URL Parameters** : `id=[objectId]` where `id` is the `_id` of the Provider on the database.

**Method** : `PUT`

**Auth required** : NO

**Permissions required** : None

**Data constraints**

```json
{
    "firstName": [ "String", "Required" ],
    "lastName": [ "String", "Required" ],
    "email": [ "Email", "Unique in Providers except current", "Required" ],
    "specialty": [ "object", "required", {
        "_id": [ "Object Id", "Required" ],
        "name": [ "String", "Required" ],
        "createdBy": [ "Integer", "Required" ],
        "createdAt": [ "Date", "Required" ],
        "updatedBy": [ "Integer", "Required" ],
        "updatedAt": [ "Date", "Required" ]
    }],
    "projectedStartDate": [ "Date", "Required" ],
    "providerType": [ "Enum", "Required", { "possibleValues": [
        "APRN",
        "ARNP",
        "CNS",
        "CRNA",
        "DC",
        "DDS",
        "DMD",
        "DO",
        "DPM",
        "LCMFT",
        "LCMHC",
        "LCP",
        "MD",
        "NP",
        "PA"
    ]}],
    "staffStatus": [ "Enum", "Required", { "possibleValues": [
        "ACTIVE",
        "AFFILIATE",
        "ASSOCIATE",
        "COMMUNITY",
        "CONSULTING",
        "COURTESY",
        "FACULTY",
        "HONORARY",
        "HOSPITALIST",
        "HOUSE_STAFF",
        "LOCUM_TENENS",
        "PROVISIONAL",
        "RESINDENT",
        "TEACHING"
    ]}],
    "status": [ "Enum", "Required", { "possibleValues": [
        "AWAITING_CREDENTIALS",
        "READY_FOR_REVIEW",
        "UNDER_REVIEW",
        "AWAITING_DECISION",
        "APPROVED",
        "DENIED"
    ], "dafaultValue": "AWAITING_CREDENTIALS"  }],
    "profilePhoto": [ "Uri" ],
    "employerId": [ "Integer" ],
    "assignedTo": [ "Integer", "Required when empoyerId exist" ],
    "updatedBy": [ "Integer", "Required" ]
}
```

**Data example** Only required fields.

```json
{
    "status": "AWAITING_CREDENTIALS",
    "firstName": "New name extra for 5",
    "lastName": "LastName",
    "email": "test5@hotmail.com",
    "specialty": {
        "_id": "00000000000000001",
        "name": "Name for test",
        "createdBy": 12345,
        "createdAt": "2020-04-11T15:54:20.898Z",
        "updatedBy": 12345,
        "updatedAt": "2020-04-11T15:54:20.898Z"
    },
    "projectedStartDate": "2019-12-17T16:48:21.620Z",
    "providerType": "NP",
    "staffStatus": "COMMUNITY",
    "updatedBy": 00002
}
```

## Success Response

**Condition** : If everything is OK.

**Code** : `200 OK`

**Content example**

```json
{
    "success": true,
    "item": {
        "status": "AWAITING_CREDENTIALS",
        "_id": "000000000000000000001",
        "firstName": "New name extra for 5",
        "lastName": "LastName",
        "email": "test5@hotmail.com",
        "specialty": {
            "_id": "00000000000000001",
            "name": "Name for test",
            "createdBy": 12345,
            "createdAt": "2020-04-11T15:54:20.898Z",
            "updatedBy": 12345,
            "updatedAt": "2020-04-11T15:54:20.898Z"
        },
        "projectedStartDate": "2019-12-17T16:48:21.620Z",
        "providerType": "NP",
        "staffStatus": "COMMUNITY",
        "createdBy": 00001,
        "createdAt": "2020-04-12T02:47:13.460Z",
        "updatedBy": 00002,
        "updatedAt": "2020-04-12T03:39:12.948Z",
        "assignedTo": 003,
        "employerId": 002,
        "profilePhoto": "https://res.cloudinary.com/super-cloud/2020-04-12T03:38:13.057Z.png"
    }
}
```

## Error Responses

**Condition** : One field ignored.

**Code** : `422 UNPROCESSABLE ENTITY`

**Content** : 

```json
{
    "success": false,
    "error": {
        "isJoi": true,
        "name": "ValidationError",
        "details": [
            {
                "message": "\"firstName\" is required",
                "path": [
                    "firstName"
                ],
                "type": "any.required",
                "context": {
                    "key": "firstName",
                    "label": "firstName"
                }
            }
        ],
        "_object": {
            ...
        }
    }
}
```

### Or

**Condition** : Field type error.

**Code** : `422 UNPROCESSABLE ENTITY`

**Content** : 

```json
{
    "success": false,
    "error": {
        "isJoi": true,
        "name": "ValidationError",
        "details": [
            {
                "message": "\"updatedBy\" must be a number",
                "path": [
                    "updatedBy"
                ],
                "type": "number.base",
                "context": {
                    "value": "1234sadf",
                    "key": "updatedBy",
                    "label": "updatedBy"
                }
            }
        ],
        "_object": {
            ...
            "updatedBy": "asdsdf",
            ...
        }
    }
}
```

### OR 

**Condition** : If Provider does not exist with `_id` of provided `id` parameter.

**Code** : `404 NOT FOUND`

**Content** : 

```json
{
    "success": false,
    "error": "Item not found",
    "status": 404
}
```

### Or

**Condition** : If the provided `id` parameter not is a valid object id.

**Code** : `422 UNPROCESSABLE ENTITY`

**Content** :

```json
{
    "success": false,
    "error": {
        "message": "Cast to ObjectId failed for value \"0000000000000000\" at path \"_id\" for model \"Providers\"",
        "name": "CastError",
        "stringValue": "\"0000000000000000\"",
        "value": "0000000000000000",
        "path": "_id",
        "reason": {}
    }
}
```

### Or

**Condition** : Select non-image file.

**Code** : `422 UNPROCESSABLE ENTITY`

**Content** :

```json
{
    "success": false,
    "error": "Invalid file type, only JPG, JPEG and PNG is allowed!"
}
```