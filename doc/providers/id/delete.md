# Delete Provider

Delete the Provider.

**URL** : `/api/v1/providers/:id/`

**URL Parameters** : `id=[objectId]` where `id` is the `_id` of the Provider on the database.

**Method** : `DELETE`

**Auth required** : NO

**Permissions required** : None

**Data** : `{}`

## Success Response

**Condition** : If the Account exists.

**Code** : `200 Ok`

**Content** : Response with the old item.

```json
{
    "success": true,
    "item": {
        "_id": "0000000000000000001",
        "firstName": "Name",
        "lastName": "LastName",
        "email": "test1@email.com",
        "specialty": {
            "_id": "00000000000000003",
            "name": "Name 3",
            "createdBy": 123,
            "createdAt": "2018-02-14T23:50:21.903Z",
            "updatedBy": 1234,
            "updatedAt": "2018-09-12T05:37:40.170Z"
        },
        "projectedStartDate": "2016-12-24T05:56:25.515Z",
        "employerId": 123,
        "providerType": "DO",
        "staffStatus": "COMMUNITY",
        "assignedTo": 12345,
        "status": "READY_FOR_REVIEW",
        "createdBy": 12345,
        "createdAt": "2017-04-22T11:21:14.160Z",
        "updatedBy": 12345,
        "updatedAt": "2017-09-12T05:18:07.647Z",
        "profilePhoto": "https://res.cloudinary.com/uri-example/2020-04-12T03:12:31.764Z.png",
        "extraField": "DONE",
        "__v": 0
    }
}
```

## Error Responses

**Condition** : If the provided `id` parameter not is a valid object id.

**Code** : `422 UNPROCESSABLE ENTITY`

**Content** :

```json
{
    "success": false,
    "error": {
        "message": "Cast to ObjectId failed for value \"0000000000000000\" at path \"_id\" for model \"Providers\"",
        "name": "CastError",
        "stringValue": "\"0000000000000000\"",
        "value": "0000000000000000",
        "path": "_id",
        "reason": {}
    }
}
```