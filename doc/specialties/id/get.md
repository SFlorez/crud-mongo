# Show Single Specialty

Show a single Specialty.

**URL** : `/api/v1/specialties/:id/`

**URL Parameters** : `id=[objectId]` where `id` is the `_id` of the Specialty on the database.

**Method** : `GET`

**Auth required** : NO

**Permissions required** : None

**Data**: `{}`

## Success Response

**Condition** : Normal request.

**Code** : `200 OK`

**Content example**

```json
{
    "success": true,
    "item": {
        "_id": "00000000000001",
        "name": "Test name",
        "createdBy": 1234,
        "createdAt": "2020-04-11T15:54:20.898Z",
        "updatedBy": 0101,
        "updatedAt": "2020-04-11T22:44:26.857Z"
    }
}
```

## Error Responses

**Condition** : If Specialty does not exist with `_id` of provided `id` parameter.

**Code** : `404 NOT FOUND`

**Content** : 

```json
{
    "success": false,
    "error": "Item not found",
    "status": 404
}
```

### Or

**Condition** : If the provided `id` parameter not is a valid object id.

**Code** : `422 UNPROCESSABLE ENTITY`

**Content** :

```json
{
    "success": false,
    "error": {
        "message": "Cast to ObjectId failed for value \"0000000000000000\" at path \"_id\" for model \"Specialties\"",
        "name": "CastError",
        "stringValue": "\"0000000000000000\"",
        "value": "0000000000000000",
        "path": "_id",
        "reason": {}
    }
}
```