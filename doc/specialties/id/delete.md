# Delete Specialty

Delete the Specialty.

**URL** : `/api/v1/specialties/:id/`

**URL Parameters** : `id=[objectId]` where `id` is the `_id` of the Specialty on the database.

**Method** : `DELETE`

**Auth required** : NO

**Permissions required** : None

**Data** : `{}`

## Success Response

**Condition** : If the Account exists.

**Code** : `200 Ok`

**Content** : Response with the old item.

```json
{
    "success": true,
    "item": {
        "_id": "00000000000001",
        "name": "Test item name",
        "createdBy": 1223,
        "updatedBy": 1223,
        "createdAt": "2020-04-11T14:15:42.971Z",
        "updatedAt": "2020-04-11T14:15:42.971Z",
        "__v": 0
    }
}
```

## Error Responses

**Condition** : If the provided `id` parameter not is a valid object id.

**Code** : `422 UNPROCESSABLE ENTITY`

**Content** :

```json
{
    "success": false,
    "error": {
        "message": "Cast to ObjectId failed for value \"0000000000000000\" at path \"_id\" for model \"Specialties\"",
        "name": "CastError",
        "stringValue": "\"0000000000000000\"",
        "value": "0000000000000000",
        "path": "_id",
        "reason": {}
    }
}
```