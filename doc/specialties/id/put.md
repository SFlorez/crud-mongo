# Update Specialty

Update a single Specialty.

**URL** : `/api/v1/specialties/:id/`

**URL Parameters** : `id=[objectId]` where `id` is the `_id` of the Specialty on the database.

**Method** : `PUT`

**Auth required** : NO

**Permissions required** : None

**Data constraints**

```json
{
    "name": [ "String", "Required" ],
    "updatedBy": [ "Integer", "Required" ]
}
```

**Data example** All fields must be sent.

```json
{
    "name": "The item name",
    "updatedBy": 1234
}
```

## Success Response

**Condition** : If everything is OK.

**Code** : `200 OK`

**Content example**

```json
{
    "success": true,
    "item": {
        "_id": "00000000000000001",
        "name": "The item name",
        "createdBy": 1234,
        "createdAt": "2020-04-12T01:27:47.840Z",
        "updatedBy": 1234,
        "updatedAt": "2020-04-12T01:27:47.840Z"
    }
}
```

## Error Responses

**Condition** : One field ignored.

**Code** : `422 UNPROCESSABLE ENTITY`

**Content** : 

```json
{
    "success": false,
    "error": {
        "isJoi": true,
        "name": "ValidationError",
        "details": [
            {
                "message": "\"name\" is required",
                "path": [
                    "name"
                ],
                "type": "any.required",
                "context": {
                    "key": "name",
                    "label": "name"
                }
            }
        ],
        "_object": {
            "createdBy": 1234,
            "createdAt": 1586655024721,
            "updatedBy": 1234,
            "updatedAt": 1586655024721
        }
    }
}
```

### Or

**Condition** : Field type error.

**Code** : `422 UNPROCESSABLE ENTITY`

**Content** : 

```json
{
    "success": false,
    "error": {
        "isJoi": true,
        "name": "ValidationError",
        "details": [
            {
                "message": "\"updatedBy\" must be a number",
                "path": [
                    "updatedBy"
                ],
                "type": "number.base",
                "context": {
                    "value": "1234sadf",
                    "key": "updatedBy",
                    "label": "updatedBy"
                }
            }
        ],
        "_object": {
            "name": "The item name",
            "createdBy": "1234sadf",
            "createdAt": 1586655247933,
            "updatedBy": "1234sadf",
            "updatedAt": 1586655247933
        }
    }
}
```

### OR 

**Condition** : If Specialty does not exist with `_id` of provided `id` parameter.

**Code** : `404 NOT FOUND`

**Content** : 

```json
{
    "success": false,
    "error": "Item not found",
    "status": 404
}
```

### Or

**Condition** : If the provided `id` parameter not is a valid object id.

**Code** : `422 UNPROCESSABLE ENTITY`

**Content** :

```json
{
    "success": false,
    "error": {
        "message": "Cast to ObjectId failed for value \"0000000000000000\" at path \"_id\" for model \"Specialties\"",
        "name": "CastError",
        "stringValue": "\"0000000000000000\"",
        "value": "0000000000000000",
        "path": "_id",
        "reason": {}
    }
}
```