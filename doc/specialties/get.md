# Show Specialties

Show all Specialties with pagination optional.

**URL** : `/api/v1/specialties/`

**Query Parameters** : 

  * `pageSize=[integer]` where `pageSize` is the number of elements per page returned.
  * `pageIndex=[integer]` where `pageIndex` is the number of the current page.

**Method** : `GET`

**Auth required** : NO

**Permissions required** : None

**Data constraints** : `{}`

## Success Responses

**Condition** : User can see all items if he does not send the page query.

**Code** : `200 OK`

**Content** : 

```json
{
    "success": true,
    "totalItems": 140,
    "pageSize": false,
    "pageIndex": false,
    "items": [
        {
            "_id": "00000000000000001",
            "name": "Name 1",
            "createdBy": 123,
            "createdAt": "2018-03-08T08:45:08.985Z",
            "updatedBy": 1234,
            "updatedAt": "2018-09-12T03:49:29.427Z"
        },
        {
            "_id": "00000000000000002",
            "name": "Name 2",
            "createdBy": 123,
            "createdAt": "2018-02-14T23:50:21.903Z",
            "updatedBy": 1234,
            "updatedAt": "2018-09-12T05:37:40.170Z"
        },
        {
            "_id": "00000000000000003",
            "name": "Name 3",
            "createdBy": 123,
            "createdAt": "2018-02-14T23:50:21.903Z",
            "updatedBy": 1234,
            "updatedAt": "2018-09-12T05:37:40.170Z"
        },
        ...
    ]
}
```


### OR

**Condition** : Request with pagination parameters `pageSize` and `pageIndex`.

**Code** : `200 OK`

**Content** : Query structure `/api/v1/specialties?pageSize=2&pageIndex=1`:

```json
{
    "success": true,
    "totalItems": 140,
    "pageSize": 2,
    "pageIndex": 1,
    "items": [
        {
            "_id": "00000000000000001",
            "name": "Name 1",
            "createdBy": 123,
            "createdAt": "2018-03-08T08:45:08.985Z",
            "updatedBy": 1234,
            "updatedAt": "2018-09-12T03:49:29.427Z"
        },
        {
            "_id": "00000000000000002",
            "name": "Name 2",
            "createdBy": 123,
            "createdAt": "2018-02-14T23:50:21.903Z",
            "updatedBy": 1234,
            "updatedAt": "2018-09-12T05:37:40.170Z"
        }
    ]
}
```