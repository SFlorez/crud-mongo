# Create Specialty

Create an Specialty.

**URL** : `/api/v1/specialties/`

**Method** : `POST`

**Auth required** : NO

**Permissions required** : None

**Data constraints**

```json
{
    "name": [ "String", "Required" ],
    "createdBy": [ "Integer", "Required" ]
}
```

**Data example** All fields must be sent.

```json
{
    "name": "The item name",
    "createdBy": 1234
}
```

## Success Response

**Condition** : If everything is OK.

**Code** : `201 CREATED`

**Content example**

```json
{
    "success": true,
    "item": {
        "_id": "00000000000000001",
        "name": "The item name",
        "createdBy": 1234,
        "createdAt": "2020-04-12T01:27:47.840Z",
        "updatedBy": 1234,
        "updatedAt": "2020-04-12T01:27:47.840Z"
    }
}
```

## Error Responses

**Condition** : One field ignored.

**Code** : `422 UNPROCESSABLE ENTITY`

**Content** : 

```json
{
    "success": false,
    "error": {
        "isJoi": true,
        "name": "ValidationError",
        "details": [
            {
                "message": "\"name\" is required",
                "path": [
                    "name"
                ],
                "type": "any.required",
                "context": {
                    "key": "name",
                    "label": "name"
                }
            }
        ],
        "_object": {
            "createdBy": 1234,
            "createdAt": 1586655024721,
            "updatedBy": 1234,
            "updatedAt": 1586655024721
        }
    }
}
```

### Or

**Condition** : Field type error.

**Code** : `422 UNPROCESSABLE ENTITY`

**Content** : 

```json
{
    "success": false,
    "error": {
        "isJoi": true,
        "name": "ValidationError",
        "details": [
            {
                "message": "\"createdBy\" must be a number",
                "path": [
                    "createdBy"
                ],
                "type": "number.base",
                "context": {
                    "value": "1234sadf",
                    "key": "createdBy",
                    "label": "createdBy"
                }
            }
        ],
        "_object": {
            "name": "The item name",
            "createdBy": "1234sadf",
            "createdAt": 1586655247933,
            "updatedBy": "1234sadf",
            "updatedAt": 1586655247933
        }
    }
}
```