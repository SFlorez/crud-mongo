# RESTAPIDocs

Documentation structure attribution: https://github.com/jamescooke/restapidocs

Where full URLs are provided in responses they will be rendered as if service is running on 'http://localhost:3001/'.

For security, the values ​​of `_id` and other fields (`name`, `firstName`, ....) were replaced by random values ​​and are not valid in many cases.

### Specialities related

Endpoints for viewing and manipulating specialities.

* [Show Specialties](./specialties/get.md) : `GET /api/v1/specialties/`
* [Create Specialty](./specialties/post.md) : `POST /api/v1/specialties/`
* [Show An Specialty](./specialties/id/get.md) : `GET /api/v1/specialties/:id/`
* [Update An Specialty](./specialties/id/put.md) : `PUT /api/v1/specialties/:id/`
* [Delete An Specialty](./specialties/id/delete.md) : `DELETE /api/v1/specialties/:id/`

### Providers related

Endpoints for viewing and manipulating providers.

* [Show Providers](./providers/get.md) : `GET /api/v1/providers/`
* [Create Provider](./providers/post.md) : `POST /api/v1/providers/`
* [Show An Provider](./providers/id/get.md) : `GET /api/v1/providers/:id/`
* [Update An Provider](./providers/id/put.md) : `PUT /api/v1/providers/:id/`
* [Delete An Provider](./providers/id/delete.md) : `DELETE /api/v1/providers/:id/`