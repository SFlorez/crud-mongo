'use strict';

const Joi = require('joi');
const mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

const validator = Joi.extend((joi) => {
  return {
    base: joi.string(),
    name: 'extendString',
    language: {
      objectId: 'not is a valid object id',
      uniqueIn: '{{v}} must by unique in {{m}}'
    },
    rules: [
      {
        name: 'objectId',
        validate(params, value, state, options) {

          if (!ObjectId.isValid(value)) {
            return this.createError('extendString.objectId', {}, state, options);
          }

          return value; // Everything is OK
        }
      },
      {
        name: 'uniqueIn',
        params: {
          //model: joi.alternatives([joi.any().required()]),
          model: joi.alternatives([joi.string().required()]),
          key: joi.alternatives([joi.string().required()]),
          error: joi.alternatives([joi.boolean().required()])
        },
        validate(params, value, state, options) {

          if (params.error) {
            return this.createError('extendString.uniqueIn', { v: value, m: params.model }, state, options);
          }

          return value; // Everything is OK
        },
        
      }
    ]
  };
});

module.exports = validator;