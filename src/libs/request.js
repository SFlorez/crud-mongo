'use strict';

const fetch = require('node-fetch');

module.exports = function request(url, options = {}) {
  return fetch(url, {
    method: options.method,
    headers: {
      'Content-Type': 'application/json',
      'accept': 'application/json',
      ...options.extraHeaders
    },
    body: JSON.stringify(options.body)
  })
    .then(res => res.json().then(data => {
      return ({status: res.status, body: data});
    }).catch(error => {
      return ({ status: 500, body: error });
    })).then(res => res);
};