'use strict';

import { config } from '../config/env';

const cloudinary = require('cloudinary').v2;
const fs = require('fs');
const cloudinaryOptions = {crop: 'fill', width: 640, height: 640, phash: true};

cloudinary.config({
  cloud_name: config.cloudinary.cloudName,
  api_key: config.cloudinary.apiKey,
  api_secret: config.cloudinary.apiSecret
});

class StoreCloud {
  static async uploadToCloudinary(image) {
    try {
      const path = image.path;
      const name = new Date().toISOString();

      if (image.path) {
        const imageUpload = await cloudinary.uploader.upload(
          image.path,
          { public_id: `providers/${name}`, tags: 'provider', ...cloudinaryOptions }
        );
        
        // remove file from server
        fs.unlinkSync(path);

        return { success: true, imageUpload };
      } 
      return { success: false, error: 'Unresolve image path' };

    } catch (error) {
      return { success: false, error };
    }
  }
}

module.exports = StoreCloud;