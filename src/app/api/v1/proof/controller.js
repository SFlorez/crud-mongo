'use strict';

function index(req, res) {
  const proofs = [{ name: 'proof 1' }, { name: 'proof 2' }, { name: 'proof 3' }];
  return res.status(200).json({ success: true, data: proofs });
}

function check(req, res) {
  return res.status(200).end();
}

module.exports = {
  index,
  check
};
