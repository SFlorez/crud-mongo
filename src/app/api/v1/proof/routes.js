'use strict';

const express = require('express');
const router = express.Router();

const proofController = require('./controller');

router.get('/', proofController.index);
router.get('/check', proofController.check);

module.exports = router;
