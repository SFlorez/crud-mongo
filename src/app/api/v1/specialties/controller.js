'use strict';

import { logger } from '../../../../config/logger';
const SpecialtyService = require('./service');

async function index(req, res) {
  try {  
    const specialtyService = new SpecialtyService();
    const pageSize = !isNaN(req.query.pageSize) ? parseInt(req.query.pageSize) : false;
    const pageIndex = !isNaN(req.query.pageIndex) ? parseInt(req.query.pageIndex) : false;
  
    const query = {
      pageSize,
      pageIndex,
      match: {}
    };
    let result = await specialtyService.getSpecialties(query);

    if (result.error) return res.status(422).json({ success: false, ...result });
    return res.status(200).json({ success: true, ...result });

  } catch (error) {
    logger.log('ERROR: SpecialtyController:index: ', error);
    return res.status(500).json({ success: false, error });
  }
}

async function getOne(req, res) {
  try {
    const { params } = req;
    const specialtyService = new SpecialtyService();

    const result = await specialtyService.getSpecialty(params.id);

    if (result.error) return res.status(result.status || 500).json({ success: false, ...result });
    return res.status(200).json({ success: true, ...result });

  } catch (error) {
    logger.log('ERROR: SpecialtyController:delete: ', error);
    return res.status(500).json({ success: false, error });
  }
}

async function create(req, res) {
  try {
    const {body} = req;
    const specialtyService = new SpecialtyService();

    const result = await specialtyService.createSpecialty(body);

    if (result.error) return res.status(422).json({ success: false, ...result });
    return res.status(201).json({ success: true, ...result });

  } catch (error) {
    logger.log('ERROR: SpecialtyController:create: ', error);
    return res.status(500).json({ success: false, error });
  }
}

async function update(req, res) {
  try {
    const { body, params } = req;
    const specialtyService = new SpecialtyService();

    const result = await specialtyService.updateSpecialty(params.id, body);

    if (result.error) return res.status(result.status || 422).json({ success: false, ...result });
    return res.status(200).json({ success: true, ...result });

  } catch (error) {
    logger.log('ERROR: SpecialtyController:update: ', error);
    return res.status(500).json({ success: false, error });
  }
}

async function remove(req, res) {
  try {
    const { params } = req;
    const specialtyService = new SpecialtyService();

    const result = await specialtyService.deleteSpecialty(params.id);

    if (result.error) return res.status(422).json({ success: false, ...result });
    return res.status(200).json({ success: true, ...result });

  } catch (error) {
    logger.log('ERROR: SpecialtyController:delete: ', error);
    return res.status(500).json({ success: false, error });
  }
}

module.exports = {
  index,
  getOne,
  create,
  update,
  remove
};
