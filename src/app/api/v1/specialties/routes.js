'use strict';

const express = require('express');
const router = express.Router();

const specialtyController = require('./controller');

router.get('/', specialtyController.index);
router.get('/:id', specialtyController.getOne);
router.post('/', specialtyController.create);
router.put('/:id', specialtyController.update);
router.delete('/:id', specialtyController.remove);

module.exports = router;