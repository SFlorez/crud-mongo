
import { logger } from '../../../../config/logger';
const Specialty = require('../../../../database/schemas/specialities');
// const ObjectId = require('mongoose').Types.ObjectId;

class SpecialtyService {
  async getSpecialties({pageSize, pageIndex, match = {}}) {
    const limit = pageSize;
    const skip = (pageSize && pageIndex) ? (pageSize * pageIndex) : false;

    var aggregation = [
      { $match: match }
    ];
    var aggregationPaginated = aggregation.slice(0);

    aggregation.push({
      $group: {
        _id: null,
        count: { $sum: 1 }
      }
    });

    if (limit) aggregationPaginated.push({ $limit: skip ? skip + limit : limit });
    if (skip) aggregationPaginated.push({ $skip: skip });
    
    try {
      const filDocuments = await Specialty.aggregate(aggregation).exec();
      var numFiltered = filDocuments[0].count;
      
      const items = await Specialty.aggregate(aggregationPaginated).exec();

      return {
        totalItems: numFiltered,
        pageSize,
        pageIndex,
        items
      };

    } catch (error) {
      logger.error('[ERROR: SpecialtyService:getSpecialties]', error);
      return { error };
    }
  }

  async getSpecialty(id) {
    try {
      // Update item
      const item = await Specialty.findOne({ _id: id });
      if (item) {
        return {
          item
        };
      }
      return { error: 'Item not found', status: 404 };
    } catch (error) {
      logger.error('[ERROR: SpecialtyService:getSpecialty]: ', error);
      return { error };
    }
  }

  async createSpecialty(body) {
    try {
      const dateNow = new Date().getTime();
      const data = { 
        name: body.name,
        createdBy: body.createdBy,
        createdAt: dateNow,
        updatedBy: body.createdBy,
        updatedAt: dateNow
      };
      var item = new Specialty(data);
      var validateError = await item.joiValidateCreate(data);
  
      if (validateError.error) {
        logger.error('[ERROR: SpecialtyService:createSpecialty]', validateError);
        return { message: 'Invalid request data', error: validateError.error.details };
      }
      // Create item
      await item.save();
      return {
        item
      };
    } catch (error) {
      logger.error('[ERROR: SpecialtyService:createSpecialty]: ', error);
      return { error };
    }
  }

  async updateSpecialty(id, body) {
    try {
      const dateNow = new Date().getTime();
      const data = { 
        name: body.name,
        updatedBy: body.updatedBy,
        updatedAt: dateNow
      };

      var validateError = Specialty.joiValidateUpdate(data);
      
      if (validateError.error) {
        logger.error('[ERROR: SpecialtyService:createSpecialty]', validateError);
        return { message: 'Invalid request data', error: validateError.error.details };
      }
      // Update item
      const item = await Specialty.findOneAndUpdate({ _id: id }, data, { new: true });
      if (item) return { item };
      return { error: 'Item not found', status: 404 };
    } catch (error) {
      logger.error('[ERROR: SpecialtyService:createSpecialty]: ', error);
      return { error };
    }
  }

  async deleteSpecialty(id) {
    try {
      const item = await Specialty.findOneAndDelete({ _id: id });
      return {
        item
      };
    } catch (error) {
      logger.error('[ERROR: SpecialtyService:deleteSpecialty]: ', error);
      return { error };
    }
  }
}

module.exports = SpecialtyService;