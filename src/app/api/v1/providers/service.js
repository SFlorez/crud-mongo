
import { logger } from '../../../../config/logger';
import { config } from '../../../../config/env';
const Provider = require('../../../../database/schemas/providers');
const StoreCloud = require('../../../../libs/storeCloud');


class ProviderService {
  async getProviders({pageSize, pageIndex, match = {}}) {
    const limit = pageSize;
    const skip = (pageSize && pageIndex) ? (pageSize * pageIndex) : false;

    var aggregation = [
      { $match: match }
    ];
    var aggregationPaginated = aggregation.slice(0);

    aggregation.push(
      {
        $group: {
          _id: null,
          count: { $sum: 1 }
        }
      }
    );
    
    if (limit) aggregationPaginated.push({ $limit: skip ? skip + limit : limit });
    if (skip) aggregationPaginated.push({ $skip: skip });
    
    // Add extra field
    aggregationPaginated.push({
      $addFields: {
        'extraField': {
          $cond: [ { '$gt': ['$projectedStartDate', new Date()] }, 'PENDING', 'DONE' ]
        }
      }
    });
    
    try {
      const filDocuments = await Provider.aggregate(aggregation).exec();
      var numFiltered = filDocuments[0].count;
      
      const items = await Provider.aggregate(aggregationPaginated).exec();

      return {
        totalItems: numFiltered,
        pageSize,
        pageIndex,
        items
      };

    } catch (error) {
      logger.error('[ERROR: ProviderService:getProviders]', error);
      return { error };
    }
  }

  async getProvider(id) {
    try {
      // Update item
      const item = await Provider.findOne({ _id: id });
      // Add extra field
      if (item) {
        var now = new Date();
        var projectedStartDate = new Date(item.projectedStartDate);
        const result = JSON.parse(JSON.stringify(item));
        return {
          item: { ...result, extraField: projectedStartDate > now ? 'PENDING' : 'DONE' }
        };
      }
      return { error: 'Item not found', status: 404 };
    } catch (error) {
      logger.error('[ERROR: ProviderService:getProvider]: ', error);
      return { error };
    }
  }

  async createProvider(body, image) {
    try {
      const dateNow = new Date().getTime();
      const data = { 
        ...body,
        createdBy: body.createdBy,
        createdAt: dateNow,
        updatedBy: body.createdBy,
        updatedAt: dateNow
      };

      if (image && config.cloudinary.enable) {
        const uploadResult = await StoreCloud.uploadToCloudinary(image);
        if (uploadResult.success && uploadResult.imageUpload){ 
          data.profilePhoto = uploadResult.imageUpload.secure_url;
        }
      }

      var item = new Provider(data);
      var validateError = await Provider.joiValidate(data);
  
      if (validateError.error) {
        logger.error('[ERROR: ProviderService:createProvider]', validateError);
        return { message: 'Invalid request data', error: validateError.error.details };
      }
      
      // Create item
      await item.save();
      
      var now = new Date();
      var projectedStartDate = new Date(item.projectedStartDate);
      const result = JSON.parse(JSON.stringify(item));

      return {
        item: { ...result, extraField: projectedStartDate > now ? 'PENDING' : 'DONE' }
      };
    } catch (error) {
      logger.error('[ERROR: ProviderService:createProvider]: ', error);
      return { error };
    }
  }

  async updateProvider(id, body, image) {
    try {
      const dateNow = new Date().getTime();
      const data = { 
        ...body,
        updatedBy: body.updatedBy,
        updatedAt: dateNow
      };

      if (image && config.cloudinary.enable) {
        const uploadResult = await StoreCloud.uploadToCloudinary(image);
        if (uploadResult.success && uploadResult.imageUpload){ 
          data.profilePhoto = uploadResult.imageUpload.secure_url;
        }
      }

      var validateError = await Provider.joiValidate(data, id);
      
      if (validateError.error) {
        logger.error('[ERROR: ProviderService:createProvider]', validateError);
        return { message: 'Invalid request data', error: validateError.error.details };
      }
      // Update item
      const item = await Provider.findOneAndUpdate({ _id: id }, data, { new: true });
      if (item) return { item };
      return { error: 'Item not found', status: 404 };
    } catch (error) {
      logger.error('[ERROR: ProviderService:createProvider]: ', error);
      return { error };
    }
  }

  async deleteProvider(id) {
    try {
      const item = await Provider.findOneAndDelete({ _id: id });
      return {
        item
      };
    } catch (error) {
      logger.error('[ERROR: ProviderService:deleteProvider]: ', error);
      return { error };
    }
  }
}

module.exports = ProviderService;