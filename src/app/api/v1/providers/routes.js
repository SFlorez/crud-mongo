'use strict';

const express = require('express');
const upload = require('../../../../middlewares/upload');
const router = express.Router();

const providerController = require('./controller');

router.get('/', providerController.index);
router.get('/:id', providerController.getOne);
router.post('/', upload.single('profilePhoto'), providerController.create);
router.put('/:id', upload.single('profilePhoto'), providerController.update);
router.delete('/:id', providerController.remove);

module.exports = router;