'use strict';

import { logger } from '../../../../config/logger';
const ProviderService = require('./service');

async function index(req, res) {
  try {  
    const providerService = new ProviderService();
    const pageSize = !isNaN(req.query.pageSize) ? parseInt(req.query.pageSize) : false;
    const pageIndex = !isNaN(req.query.pageIndex) ? parseInt(req.query.pageIndex) : false;
  
    const query = {
      pageSize,
      pageIndex,
      match: {}
    };
    let result = await providerService.getProviders(query);

    if (result.error) return res.status(422).json({ success: false, ...result });
    return res.status(200).json({ success: true, ...result });

  } catch (error) {
    logger.log('ERROR: ProviderController:index: ', error);
    return res.status(500).json({ success: false, error });
  }
}

async function getOne(req, res) {
  try {
    const { params } = req;
    const providerService = new ProviderService();

    const result = await providerService.getProvider(params.id);

    if (result.error) return res.status(result.status || 500).json({ success: false, ...result });
    return res.status(200).json({ success: true, ...result });

  } catch (error) {
    logger.log('ERROR: ProviderController:delete: ', error);
    return res.status(500).json({ success: false, error });
  }
}

async function create(req, res) {
  try {
    if (req.fileValidationError) {
      return res.status(422).json({ success: false, error: req.fileValidationError });
    }

    const { body } = req;
    const providerService = new ProviderService();

    const data = {
      ...body
    };
    if (body.specialty && typeof body.specialty === 'string') data.specialty = JSON.parse(body.specialty);

    const result = await providerService.createProvider(data, req.file);

    if (result.error) return res.status(422).json({ success: false, ...result });
    return res.status(201).json({ success: true, ...result });

  } catch (error) {
    logger.log('ERROR: ProviderController:create: ', error);
    return res.status(500).json({ success: false, error });
  }
}

async function update(req, res) {
  try {
    if (req.fileValidationError) {
      return res.status(422).json({ success: false, error: req.fileValidationError });
    }

    const { body, params } = req;
    const providerService = new ProviderService();

    const data = {
      ...body
    };
    if (body.specialty && typeof body.specialty === 'string') data.specialty = JSON.parse(body.specialty);

    const result = await providerService.updateProvider(params.id, data, req.file);

    if (result.error) return res.status(result.status || 422).json({ success: false, ...result });
    return res.status(200).json({ success: true, ...result });

  } catch (error) {
    logger.log('ERROR: ProviderController:update: ', error);
    return res.status(500).json({ success: false, error });
  }
}

async function remove(req, res) {
  try {
    const { params } = req;
    const providerService = new ProviderService();

    const result = await providerService.deleteProvider(params.id);

    if (result.error) return res.status(422).json({ success: false, ...result });
    return res.status(200).json({ success: true, ...result });

  } catch (error) {
    logger.log('ERROR: ProviderController:delete: ', error);
    return res.status(500).json({ success: false, error });
  }
}

module.exports = {
  index,
  getOne,
  create,
  update,
  remove
};
