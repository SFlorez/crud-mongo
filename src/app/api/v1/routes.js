'use strict';

const express = require('express');
const router = express.Router();

const proofRoutes = require('./proof/routes');
const specialtiesRoutes = require('./specialties/routes');
const providersRoutes = require('./providers/routes');

router.use('/proofs', proofRoutes);
router.use('/specialties', specialtiesRoutes);
router.use('/providers', providersRoutes);

module.exports = router;
