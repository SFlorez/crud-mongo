// uploadMiddleware.js

const fileFilter = (req, file, cb) => {
  let filetypes = /jpeg|jpg|png/;
  let mimetype = filetypes.test(file.mimetype);
  if (mimetype) {
    return cb(null, true);
  } else {
    req.fileValidationError = 'Invalid file type, only JPG, JPEG and PNG is allowed!';
    return cb(null, false, req.fileValidationError);
  }
};

const multer = require('multer');

const upload = multer({
  fileFilter,
  dest: './temp'
});

module.exports = upload;