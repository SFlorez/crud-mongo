'use strict';

var mongoose = require('mongoose');
var Joi = require('../../libs/validator');

var Schema = mongoose.Schema;
//  ObjectId = Schema.ObjectId;

var specialty = new Schema({
  name: String,
  createdBy: Number,
  createdAt: { type: Date, default: Date.now },
  updatedBy: Number,
  updatedAt: { type: Date, default: Date.now },
},
{ 
  versionKey: false 
});

specialty.methods.joiValidateCreate = function (obj) {
  var schema = {
    name: Joi.string().required(),
    createdBy: Joi.number().integer().required(),
    createdAt: Joi.date(),
    updatedBy: Joi.number().integer(),
    updatedAt: Joi.date(),
  };
  return Joi.validate(obj, schema);
};

specialty.statics.joiValidateUpdate = function (obj) {
  var schema = {
    name: Joi.string().required(),
    createdBy: Joi.number().integer(),
    createdAt: Joi.date(),
    updatedBy: Joi.number().integer().required(),
    updatedAt: Joi.date(),
  };
  return Joi.validate(obj, schema);
};
 
module.exports = mongoose.model('Specialties', specialty);
