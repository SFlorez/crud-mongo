'use strict';

var mongoose = require('mongoose');
var Joi = require('../../libs/validator');
var Specialty = require('./specialities');

var Schema = mongoose.Schema;

const providerTypeEnum = [
  'APRN',
  'ARNP',
  'CNS',
  'CRNA',
  'DC',
  'DDS',
  'DMD',
  'DO',
  'DPM',
  'LCMFT',
  'LCMHC',
  'LCP',
  'MD',
  'NP',
  'PA'
];

const staffStatusEnum = [
  'ACTIVE',
  'AFFILIATE',
  'ASSOCIATE',
  'COMMUNITY',
  'CONSULTING',
  'COURTESY',
  'FACULTY',
  'HONORARY',
  'HOSPITALIST',
  'HOUSE_STAFF',
  'LOCUM_TENENS',
  'PROVISIONAL',
  'RESINDENT',
  'TEACHING'
];

const statusEnum = [
  'AWAITING_CREDENTIALS',
  'READY_FOR_REVIEW',
  'UNDER_REVIEW',
  'AWAITING_DECISION',
  'APPROVED',
  'DENIED'
];

var provider = new Schema({
  firstName: String,
  lastName: String,
  email: { type: String,  unique: true },
  specialty: Specialty.schema,
  projectedStartDate: { type: Date },
  providerType: { 
    type: String, 
    enum: providerTypeEnum
  },
  staffStatus: { 
    type: String, 
    enum: staffStatusEnum
  },
  status: { 
    type: String, 
    default: 'AWAITING_CREDENTIALS',
    enum: statusEnum
  },
  profilePhoto: String,
  employerId: Number,
  assignedTo: Number,
  createdBy: Number,
  createdAt: { type: Date, default: Date.now },
  updatedBy: Number,
  updatedAt: { type: Date, default: Date.now },
},
{ 
  versionKey: false 
});
 
provider.statics.joiValidate = async function (obj, id) {
  // validate unique
  const key = 'email';
  const model = 'Providers';
  const items = await this.model(model).find({ $and: [ { [key]: obj[key] }, { _id: { $ne: id } } ]}).exec();
  var uniqueError = false;
  if(items.length > 0) uniqueError = true;

  var schema = {
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.extendString().email().uniqueIn(model, key, uniqueError).required(),
    specialty: Joi.object({
      _id: Joi.extendString().objectId().required(),
      name: Joi.string().required(),
      createdBy: Joi.number().integer().required(),
      createdAt: Joi.date().required(),
      updatedBy: Joi.number().integer().required(),
      updatedAt: Joi.date().required()
    }).required(),
    projectedStartDate: Joi.date().required(),
    providerType: Joi.string().valid(providerTypeEnum).required(),
    staffStatus: Joi.string().valid(staffStatusEnum).required(),
    status: Joi.string().valid(statusEnum).required(),
    profilePhoto: Joi.string().uri(),
    employerId: Joi.number().integer(),
    assignedTo: Joi.number().integer().when('employerId', { is: Joi.exist(), then: Joi.required() }),
    createdBy: id ? Joi.number().integer() : Joi.number().integer().required(),
    createdAt: Joi.date(),
    updatedBy: id ? Joi.number().integer().required() : Joi.number().integer(),
    updatedAt: Joi.date(),
  };

  return Joi.validate(obj, schema);
};
 
module.exports = mongoose.model('Providers', provider);
