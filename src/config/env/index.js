'use strict';

const envConfig = require('./variables.js');

const defaults = {
  serverPort: process.env.SERVER_PORT || '3001',
  env: process.env.NODE_ENV || 'development',
  baseUrl: process.env.BASE_URL || 'http://localhost'
};

const config = Object.assign(envConfig, defaults);

export { config };