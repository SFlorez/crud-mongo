'use strict';

const db = {
  host: process.env.CRUD_DB_HOST || 'localhost',
  port: process.env.CRUD_DB_PORT || '19748',
  database: process.env.CRUD_DB_NAME || 'test',
  password: process.env.CRUD_DB_PASSWORD || '',
  user: process.env.CRUD_DB_USER || 'test',
};

const cloudinary = {
  enable: process.env.CRUD_ENABLE_CLOUDINARY || false,
  cloudName: process.env.CRUD_CLOUDINARY_CLOUD_NAME,
  apiKey: process.env.CRUD_CLOUDINARY_API_KEY,
  apiSecret: process.env.CRUD_CLOUDINARY_API_SECRET
};

module.exports = {
  mongo: db,
  cloudinary
};