'use strict';

import 'dotenv/config';
import { config } from './config/env';
import { app } from './app';
import { logger } from './config/logger';

const apiRoutesV1 = require('./app/api/v1/routes');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const bodyParser = require('body-parser');

app.use(helmet());
app.use(bodyParser.json());
app.use(cors());
app.use(morgan('combined'));

app.get('/', (req, res) => {
  res.send('CRUD APP WELCOME check API in /api/v1');
});

/* Set up mongoose connection */
var mongoose = require('mongoose');
var mongoDB  = `mongodb://${config.mongo.user}:${config.mongo.password}@${config.mongo.host}:${config.mongo.port}/${config.mongo.database}`;
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

/* Routes */
app.use('/api/v1', apiRoutesV1);

app.use((err, req, res) => {
  if(403 == err.status || 401 == err.status) {
    res.status(err.status).json({Error: 'Unauthorized'});
  }
});

app.listen(config.serverPort, () => {
  logger.info(`Server ${config.env} api listening on port ${config.serverPort}`);
});